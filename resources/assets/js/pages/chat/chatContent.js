import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import ChatService from '../../chatService';
import io from 'socket.io-client';
import $ from 'jquery';
const socket = io('http://meichat.com/');
class ChatContent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
		this.showDelete = this.showDelete.bind(this);
		this.softDelete = this.softDelete.bind(this);
		this.permanentDelete = this.permanentDelete.bind(this);

	}
 componentWillMount(){
	 var height = $(document).height() + 1000;
	  $(".messages").scrollTop(height);
	$(document).on('click', '.close-modal', function() {
	$('#deleteModal').hide();
});
 }
 componentDidMount(){
	var page = 1;
	$(document).on('click','.contact',function(){
		page = 1;
	})
	$('#logchat').scroll(function() {
		var pos = $('#logchat').scrollTop();
		if (pos == 0) {
			page++;
			// alert(page);
			 this.props.loadMess(this.props.conversation_id,page);
		}
	}.bind(this));
 }
 showDelete(event){
	 	$('#deleteModal').show();
	var mess_id = event.currentTarget.dataset.id;
	var author_id = event.currentTarget.dataset.author;
	this.setState({mess_del: mess_id});
	this.setState({author_del: author_id});
 }
 softDelete(event){

	 var id = event.currentTarget.dataset.id;
	 	ChatService.deleteMessageSoft(id, this.props.cr_user_id, function(response) {
			if (response == 1) $(`#${id}`).remove();
			else alert('You can not delete this message!');
		});
		$('#deleteModal').hide();
 }
  permanentDelete(event){
		if (!confirm('You will delete permanently?!')) {
			return;
		} 
	   var id = event.currentTarget.dataset.id;
	   var author_id =  this.state.author_del;

		if(author_id != this.props.cr_user_id){
			alert('You can not delete this message permanently!');
		return;
	}
	ChatService.deleteMessagePermanent(id, function(response){
		if(response== 1)
			$(`#${id }`).remove();
			else alert('You can not delete this message permanently!');
	})

		$('#deleteModal').hide();
 }

	render() {
		// this.loadMess();
	const input =  this.props.conversation_id ? <div className="message-input">
				<div className="wrap">
					<input type="text" placeholder="Write your message..." onKeyDown={this.props.onKey} id="mess-input"/>
					<i className="fa fa-paperclip attachment" aria-hidden="true" />
					<button className="submit" onClick={this.props.onSubmit}>
						<i className="fa fa-paper-plane" aria-hidden="true" />
					</button>
				</div>
			</div> : ' ';

var messages = this.props.messages;
 var cr_user = this.props.cr_user_id;
 var conversation_name = this.props.conversation_name;
 messages = messages.map(function (message, index) {
			if (message.author_id == cr_user) {
				return <li className="sent" key={index} data-author={message.author_id} id={message.id} onClick={this.showDelete} data-id={message.id}>
						<img src="images/girl.jpg" alt="avt" />
						<p>{message.body}</p>
					</li>;
			} else return <li className="replies" key={index} id={message.id} data-author={message.author_id} onClick={this.showDelete} data-id={message.id}>
							<img src="images/boy2.jpg" alt="avt" />
							<p>{message.body}</p>
						</li>;
		}.bind(this));


		return <div className="content col-sm-7">
				<div className="contact-profile">
					<span id="user_chat">
						<img src="images/boy2.jpg" alt="avt" />
						<p id="user_chat_name">{conversation_name}</p>
					</span>
				</div>
				<div className="messages" id="logchat">
					<ul id="messages-content">{messages}</ul>
				</div>
				{input}
				<div id="deleteModal" className="modal sm-modal" role="dialog">
					<div className="modal-dialog">
						<div className="modal-content">
							<div className="modal-header">
								<button type="button" className="close close-modal" data-dismiss="modal">
									×
								</button>
								<h4 className="modal-title">Choose delete</h4>
							</div>
							<div className="modal-body">
								<button className="btn " data-id={this.state.mess_del} id="soft-delete" onClick={this.softDelete} style={{ marginRight: 15 }}>
									{' '}
									Soft Delete
								</button>
								<button className="btn " data-id={this.state.mess_del} id="permanent-delete" onClick={this.permanentDelete}>
									{' '}
									Permanent delete{' '}
								</button>
							</div>
							<div className="modal-footer">
								<button type="button" className="btn btn-info  save-group close-modal" onClick={this.props.saveGroup} data-users={this.state.user_list}>
									Save
								</button>
								<button type="button" className="btn btn-default close-modal" data-dismiss="modal">
									Close
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>;
	}
}

export default ChatContent;
