<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $table = "messages";
    protected $fillable = ['body', 'author_id', 'conversation_id', 'user_deleted_array'];
}
