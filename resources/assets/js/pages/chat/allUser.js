import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import ChatService from '../../chatService';
import $ from 'jquery';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
class AllUser extends React.Component {
	constructor(props) {
        super(props);
		this.state = { users  : [],
		matchValue: true,
		matchPos: 'any',
			matchLabel: true,
			user_list: null,
			};
		this.gChange = this.gChange.bind(this);
	}
	gChange(value){
		this.setState({ user_list: value });
	}
	componentWillMount() {
        // ChatService.loadUser(function(response){
        //     this.setState({ users: response });
		// }.bind(this));
		$(document).on('click', '.contact', function() {
			$('.contact').removeClass('active');
			var friend_id =  $(this).attr("id");
			$(this).addClass('active');
		});
		$(document).on('click', '#addcontact', function() {
			$('#listusermodal').show();
		});
		$(document).on('click', '.close-modal', function() {
			$('#listusermodal').hide();
		});
		$(document).on('click', '.save-group', function() {
			$('select').val('');
		});
	
	
			}

	render() {
		var users = this.props.users;
			if (users && users != '' && users != -1) 
{
         var side_users = users.map(function(user) {
			return <li className="contact" data-id={user.id} onClick={this.props.action} key={user.id}>
					<div className="wrap">
						<span className="contact-status online" />
						<img src="images/boy2.jpg" alt="avt" />
						<div className="meta">
							<p className="name">{user.name}</p>
						</div>
					</div>
				</li>;
		}.bind(this));
		var select_users =  users.map(function(user, index) {
			return { value: user.id , label: user.name }
		});
	}
	else users = null;

return (
			<span>
				<div id="sidepanel" className="col-sm-2 sidepanel ">
					<h3 className="side-title">All Users</h3>
					<div id="contacts">
						<ul>
                            {side_users}
						</ul>
					</div>
					<div id="bottom-bar">
						<button id="addcontact"  data-toggle="modal" data-target="#listusermodal" ><i className="fa fa-user-plus fa-fw"></i> <span>Add Group</span></button>
					</div>
				</div>
		<div id="listusermodal" className="modal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close close-modal" data-dismiss="modal">×</button>
              <h4 className="modal-title">Add user in new group</h4>
            </div>
            <div className="modal-body">
					<Select
					multi
					onChange={this.gChange}
					options={select_users}
					placeholder="Add user into group"
					simpleValue	
					value={this.state.user_list}
				/>
            </div>
            <div className="modal-footer">
			  <button type="button" className="btn btn-info  save-group close-modal" onClick ={this.props.saveGroup} data-users = {this.state.user_list}>Save</button>
              <button type="button" className="btn btn-default close-modal" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
			</span>
		);
	}
}

export default AllUser;
