import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import {Validator} from 'ree-validate'
import AuthService from '../../services';
import Header from '../Header';
import Footer from '../Footer';
class Register extends React.Component {
    constructor(props) {
        super(props);
        this.validator = new Validator({
			name: 'required|min:3',
			email: 'required|email',
			password: 'required|min:6',
			agree: 'required',
			password_confirmation: 'required|min:6|confirmed:password',
		});
        this.state = {
				isAuthenticated : AuthService.checkLogin(),
            credentials: {
                name: '',
                email: '',
                password: '',
                password_confirmation: ''
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
            isSuccess: false,
            isLoading: false,
            errors: this.validator.errorBag
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        const {credentials} = this.state;
        credentials[name] = value;

        this.validator.validate(name, value)
            .then(() => {
                const {errorBag} = this.validator;
                this.setState({errors: errorBag, credentials})
            });
    }

    handleSubmit(event) {
        event.preventDefault();

        const {credentials} = this.state;

        this.validator.validateAll(credentials)
            .then(success => {
                if (success) {
                    this.setState({
                        isLoading: true
                    });
                    this.submit(credentials);
                }
            });
    }

    submit(credentials) {
        AuthService.register(credentials, function(response) {
				if (response == true) {
					this.setState({ isSuccess: true });
					return;
				}
				const responseError = { isError: true, code: response.statusCode, text: response.error };
				this.setState({
					responseError: { isError: true, code: response.statusCode, text: response.error },
				});
			}.bind(this));

    }

    componentDidMount() {
        this.setState({
            isLoading: false
        });
    }

    render() {
        const isAuthenticated  =this.state.isAuthenticated;
        if (isAuthenticated) {
			return <Redirect to="/" replace />;
		}
        const {errors} = this.state;
        return <div className="auth-content">
				<Header />
				<div className="row">
					<div className="col-sm-6 col-sm-offset-1 form-box">
						<div>
							{/* show error: */}

							{this.state.responseError.isError && <p>{this.state.responseError.text}</p>}
							{this.state.isSuccess && <p>
									Registered Successfully ! <Link to="/login">Login</Link> here
								</p>}

							<div className="form-top">
								<div className="form-top-left">
									<h3>
										<b className="title color">Register an account </b>or
										<Link to="/login"> Login</Link>
									</h3>
								</div>
								<div className="form-top-right">
									<i className="fa fa-key" />
								</div>
							</div>
							<div className="form-bottom">
								<form onSubmit={this.handleSubmit} className="login-form">
									<div className="form-group">
										<label className="sr-only">Name</label>
										<input name="name" placeholder="Name" onChange={this.handleChange} className=" form-control" type="text" />
										{errors.has('name') && <div size="tiny" className="custom-error" color="red">
												{errors.first('name')}
											</div>}
									</div>
									<div className="form-group">
										<input name="email" placeholder="E-mail address" onChange={this.handleChange} className=" form-control" type="email" />
										{errors.has('email') && <div size="tiny" className="custom-error" color="red">
												{errors.first('email')}
											</div>}
									</div>
									<div className="form-group">
										<input name="password" placeholder="Password" type="password" onChange={this.handleChange} className="form-control" />
										{errors.has('password') && <div size="tiny" className="custom-error" color="red">
												{errors.first('password')}
											</div>}
									</div>
									<div className="form-group">
										<input name="password_confirmation" placeholder="Confirm password" type="password" onChange={this.handleChange} className=" form-control" />
										{errors.has('password_confirmation') && <div size="tiny" className="custom-error" color="red">
												{errors.first('password_confirmation')}
											</div>}
									</div>
									<div className="form-group">
										<input type="checkbox" onChange={this.handleChange} name="agree" value="1" required />
										<label style={{ marginLeft: 10 }}>I agreed to Funny Chat terms </label>
									</div>
									<button color="teal" size="large" onClick={this.handleSubmit} className="btn">
										Sign Up
									</button>
									<div className="social-login-buttons">
										<a className="btn btn-link-1 btn-link-1-google-plus full-width" href="/social/google">
											<i className="fa fa-google-plus" /> Sign Up with Google
										</a>
									</div>
	
								</form>
							</div>
						</div>
					</div>
				</div>
				<Footer />
			</div>;
    }
}



export default Register;