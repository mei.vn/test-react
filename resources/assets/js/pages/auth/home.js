import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import AuthService from '../../services';
import Header from '../Header';
import Footer from '../Footer';
class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			isAuthenticated : AuthService.checkLogin(),
		}
	}

	render() {
		// 	if (!isAuthenticated) {
		// 	return <Redirect to={'/login'} replace />;
		// // }
		return <div>
				<div className="auth-content">
					<Header />
					<Footer />
				</div>
			</div>;
	}
}

export default Home;
