<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Services\PayUService\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */  

    public function checkLogin(){
        $check = Auth::check();

        echo $check;

    }
    public function login(Request $request)
    {
        // grab credentials from the request
        $remember = $request->remember;
        $credentials = $request->only('email', 'password');
     
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = Auth::attempt($credentials, $remember)) {
                return response()->json([
                    "error" => "invalid_credentials",
                    "message" => "Email or password incorrect!"
                ], 401);
            }
        } catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                "error" => "could_not_create_token",
                "message" => "Enable to process request."
            ], 422);
        }

        $user= Auth::user();
        return response()->json([
            'user'  => $user,
            'token' => $token
        ],200);

    }

    public function socialLogin($social)
    {
        if ($social == "facebook" || $social == "google") {
            return Socialite::driver($social)->stateless()->redirect();
        } else {
            return Socialite::driver($social)->redirect();           
        }
    }

    public function handleProviderCallback($social)
    {
        if ($social == "facebook" || $social == "google") {
            $userSocial = Socialite::driver($social)->stateless()->user();
        } else {
            $userSocial = Socialite::driver($social)->user();           
        }
        
        $token = $userSocial->token;
        $email = $userSocial->getEmail();
      
        $user = User::firstOrNew(['email' => $email]);

        if (!$user->id) {
            $channel = md5(str_random(8));
            $password = bcrypt(str_random(6));
            $user->fill(["name" => $userSocial->getName(),"password"=> $password, 'channel' => $channel ]);
            $user->save();
            Auth::attempt(['email' => $email, 'password' => $password]);
        }
        Auth::login($user, true);
        return response()->json([
            'user'  => $user,
            'userSocial'  => $userSocial,
            'token' => $token,
        ],200);
    }
    public function logout()
{
    echo Auth::logout();
}

}
