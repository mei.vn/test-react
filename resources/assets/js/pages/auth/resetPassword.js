import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import {Validator} from 'ree-validate'
import AuthService from '../../services'
import Header from '../Header';
import Footer from '../Footer';
class Reset extends React.Component {
	constructor(props) {
		super(props);
		this.validator = new Validator({
			password: 'required|min:6',
			password_confirmation: 'required|min:6|confirmed:password',
			token: 'required',
			email: 'required',
		});
		this.state = {
			isAuthenticated : AuthService.checkLogin(),
			credentials: {
				password: '',
				password_confirmation: '',
				token: this.props.match.params.token,
				email: this.props.match.params.email.replace('29gnmLTv686QsnV', '@'),
			},
			responseError: {
				isError: false,
				code: '',
				text: '',
			},
			isSuccess: false,
			errors: this.validator.errorBag,
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		const name = event.target.name;
		const value = event.target.value;
		const { credentials } = this.state;
		credentials[name] = value;

		this.validator.validate(name, value).then(() => {
			const { errorBag } = this.validator;
			this.setState({ errors: errorBag, credentials });
		});
	}

	handleSubmit(event) {
		event.preventDefault();

		const { credentials } = this.state;

		this.validator.validateAll(credentials).then(success => {
			if (success) {
				this.submit(credentials);
			}
		});
	}

	submit(credentials) {
    AuthService.updatePassword(credentials, function(response){
   			if (response == true) {
					this.setState({ isSuccess: true });
					return;
				}
				const responseError = { isError: true, code: response.statusCode, text: response.error };
				this.setState({
					responseError: { isError: true, code: response.statusCode, text: response.error },
                });
         }.bind(this));
	}

	render() {
        const isAuthenticated = this.state.isAuthenticated;
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const {errors} = this.state;
        if (isAuthenticated) {
            return <Redirect to={from} replace />;
        }
		return <div className="auth-content">
				<Header />
				<div className="row">
					<div className="col-sm-6 col-sm-offset-1 form-box">
						{this.state.responseError.isError && <p className="White">
								<b>{this.state.responseError.text}</b>
							</p>}
						{this.state.isSuccess && <p>
								Reset Successfully ! <Link to="/login" replace>
									Login
								</Link> here
							</p>}
						<div>
							<div className="form-top">
								<div className="form-top-left">
									<h3>
										<b className="title color">Reset your password</b>
									</h3>
								</div>
								<div className="form-top-right">
									<i className="fa fa-key" />
								</div>
							</div>
						</div>
						<div className="form-bottom">
							<form onSubmit={this.handleSubmit} className="login-form">
								<div className="form-group">
									<input name="password" placeholder="New password" type="password" onChange={this.handleChange} className="form-control" required minLength={6} />
									{errors.has('password') && <p className="custom-error">
											{errors.first('password')}
										</p>}
								</div>
								<div className="form-group">
									<input name="password_confirmation" placeholder="Confirm new password" type="password" onChange={this.handleChange} className="form-control" required minLength={6} />
									{errors.has('password_confirmation') && <p className="custom-error">
											{errors.first('password_confirmation')}
										</p>}
								</div>
								<button className="btn" type="submit">
									Change Password
								</button>
							</form>
						</div>
					</div>
				</div>
				<Footer />
			</div>;
	}
}


export default Reset;