<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
              <title>Funny chat</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
          <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"> 
          <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>
        <link rel="stylesheet" href="{{asset('css/style.css')}}"> 
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
           <link rel="stylesheet" href="{{asset('css/chat.css')}}"> 
        <link rel="stylesheet" href="{{asset('css/custom.css')}}"> 
        {{--  <link rel="stylesheet" href="{{asset('css/all.css')}}">   --}}
    </head>
    <body >
    <div class = "wrapper">
                <div id='app'></div>
    </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
