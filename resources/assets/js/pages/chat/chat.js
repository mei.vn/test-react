import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import ChatService from '../../chatService';
import AuthService from '../../services';
import Header from '../Header';
import Footer from '../Footer';
import Conversation from './chatContent';
import AllUser from './allUser';
import AllConversation from './allConversation';
import io from 'socket.io-client';
import $ from 'jquery';

class ChatContent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isAuthenticated : AuthService.checkLogin(),
			socket: null,
			cr_user: null,
			conversation_id: '',
			all_users: [],
			conversation_name: '',
			conversations: [],
			messages:[],
		};
		this.newConversation = this.newConversation.bind(this);
		this.ChangeConversation = this.ChangeConversation.bind(this);
		this.keyChange = this.keyChange.bind(this);
		this.saveMessageSubmit = this.saveMessageSubmit.bind(this);
		this.saveGroup = this.saveGroup.bind(this);
		this.markAsSeen = this.markAsSeen.bind(this);
		this.loadMess = this.loadMess.bind(this);
		
	}

	createConversation() {}
	componentWillMount() {
			$(document).on('click', '.contact', function() {
				$(this).removeClass('unread');
			});
		const cr_user = AuthService.getUserLogin();
		this.setState({ cr_user });
		// get all user
		this.loadUsers();
		this.loadConversations();

	}
	loadUsers(){
		ChatService.loadUser(function (response) {
			this.setState({
				all_users: response
			});
	
		}.bind(this));
	}
	loadConversations(){
		ChatService.loadConversation(function (response) {
			// console.log(response);
		if (response !== '-1'){
		
		 this.setState({ conversations: response });
				}
		}.bind(this));
	}
	saveGroup(event){
	var users_list = event.currentTarget.dataset.users;
	var cr_user_id = this.state.cr_user.id;
	// users_list += ','+ cr_user_id;
	users_list = users_list.split(',');
	var type = 0;
	if(users_list.length > 1){
	 type = 1;
	};
	users_list.push(this.state.cr_user.id);
		ChatService.newConversation(
			users_list, type,
			function(response) {
				if (response.status == 'success') {
					this.loadMess(response.data,1);
					var conversation_name = this.mapConversationName(response.name);
					this.setState({ conversation_name });
					this.setState({ conversation_id: response.data });
						if (response.message == 'new') {
						this.loadConversations();
					}
				}
		
			}.bind(this)
		);
	}
	ChangeConversation(event) {
		var conversation_id = event.currentTarget.dataset.id;
		var conversation_name = event.currentTarget.dataset.cv_name;
		conversation_name = this.mapConversationName (conversation_name);
		this.setState({conversation_name});
		this.setState({conversation_id});
		this.loadMess(conversation_id,1);
		this.markAsSeen(conversation_id, this.state.cr_user.id );
		var objDiv = document.getElementById("logchat");
		objDiv.scrollTop = objDiv.scrollHeight;
		
	
	}
	mapConversationName (conversation_name){
		var conversations_name = JSON.parse(conversation_name);
		var tmp_name = '';
		for (let vl_name of conversations_name) {
			if (vl_name.id != this.state.cr_user.id) {
				tmp_name += vl_name.name + '_';
			}
		}
		if (tmp_name.endsWith('_')) {
			tmp_name = tmp_name.substring(0, tmp_name.length - 1);
		}	
		return 	tmp_name;

	}
	newConversation( event) {
		var user_id = this.state.cr_user.id;
		var friend_id = event.currentTarget.dataset.id;
		var user_list = [user_id, friend_id];
		var type = 0;
		ChatService.newConversation(
			user_list, type,
			function(response) {
				
				if (response.status == 'success') {
					this.loadMess(response.data,1);
					var conversation_name = this.mapConversationName (response.name);
					this.setState({ conversation_name});
					this.setState({ conversation_id: response.data });
						if (response.message == 'new') {
						this.loadConversations();
					}
				}
		
			}.bind(this)
		);

	}
	loadMess(conversation_id, page) {
		ChatService.loadMessagesByConversationId(conversation_id, page, function (response) {
			response.reverse();
			this.setState({
				messages: response
			});
			// console.log(response);
		}.bind(this));
	}

	keyChange(event) {
		const value = event.target.value;
		if (event.which == 13 && value) {
			this.saveMess(value);
		}
	}
	saveMessageSubmit(event) {
		event.preventDefault();
		var input = document.getElementById('mess-input').value;
		this.saveMess(input);
	}
	saveMess(body) {
		if (body == '') {
			return false;
		}
		var message = {
			author_id: this.state.cr_user.id,
			conversation_id: this.state.conversation_id,
			body,
		};
		ChatService.saveMessage(message, function (response) {});

		// var messages = this.state.messages;
		// messages.push(message);
		// this.setState({
		// 	messages
		// });
		var input = document.getElementById('mess-input');
		input.value = '';
		// var height = $(".messages").scrollHeight

		// save mess in ajax
	}
	markAsSeen(conversation_id, user_id){
		ChatService.markAsSeen(conversation_id, user_id, function(response){
			if (response != true) 
			console.log(response);
		});

	}
	componentDidMount() {
		console.log(this.state.cr_user.channel);
			 const socket = io('http://localhost:6969');
				socket.on(this.state.cr_user.channel, function(data) { 
				var messages = this.state.messages;
				
						data = JSON.parse(data);
					
						
						// console.log(data);
						if(this.state.conversation_id == data.conversation_id)
						{
							messages.push(data);

						}
						else {
							this.setState({conversation_noti : data.conversation_id})
						}
						
					
						this.setState({ messages});
						var height = $(".messages").height();
						$(".messages").animate({ scrollTop:  $(document).height()+ 1000 }, "fast");

					
				}.bind(this));
	}
	render() {
	var isAuthenticated = this.state.isAuthenticated;
	if (!isAuthenticated) {
		return <Redirect to={'/login'} replace />;
	}

 	const {cr_user} = this.state;
		return<div>
				<Header />
				<div className="main-content">
					<div id="frame">
				<AllConversation action={this.ChangeConversation} cr_user_id={cr_user.id} users={this.state.all_users} conversations={this.state.conversations} conversation_active={this.state.conversation_id}   noti={this.state.conversation_noti}/>
				<Conversation cr_user_id={cr_user.id} conversation_id={this.state.conversation_id} conversation_name={this.state.conversation_name}  messages={this.state.messages} onKey={this.keyChange} onSubmit={this.saveMessageSubmit} loadMess={this.loadMess} />
				<AllUser action={this.newConversation} cr_user_id={cr_user.id} users={this.state.all_users} saveGroup={this.saveGroup} />
			</div>
				</div>
				<Footer />
			</div>;
	}
}

export default ChatContent;
