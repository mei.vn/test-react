<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
        public function test_register_wrong_should_return_error(){

          $credentials = array(
              'email' => 'invalid_email',
            'password' => 'invalid_pass',
            'name' =>'invalid_name',
            '_token' => csrf_token()
        );
        $response = $this->call('post', 'auth/register', $credentials);
        $result['error'] = "validation_error";
          $result = [
              'error' => "validation_error",
            'message' => [
                'email' => ['The email must be a valid email address.'],
                'password' => [ 'The password confirmation does not match.']
            ]];
        $result = json_encode($result);
        $this->assertEquals($result , $response->getContent());
    }
    //  public function test_register_success_should_return_noti(){

    //       $credentials = array(
    //           'email' => 'thuong1@a.com',
    //         'password' => '123456',
    //         'password_confirmation' => '123456',
    //         'name' =>'thuong',
    //         '_token' => csrf_token()
    //     );
    //     $response = $this->call('post', 'auth/register', $credentials);
    //     $result = ['status','registered successfully'];
    //     $result = json_encode($result);
    //     $this->assertEquals($result , $response->getContent());
    // }
}
