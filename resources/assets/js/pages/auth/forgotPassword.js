import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Validator } from 'ree-validate';
import AuthService from '../../services';
import Header from '../Header';
import Footer from '../Footer';
class Forgot extends React.Component {
	constructor(props) {
		super(props);
		this.validator = new Validator({
			email: 'required|email',
		});

		this.state = {
			isAuthenticated: AuthService.checkLogin(),
			credentials: {
				email: '',
			},
			responseError: {
				isError: false,
				code: '',
				text: '',
			},
			isSuccess: false,
			errors: this.validator.errorBag,
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		const name = event.target.name;
		const value = event.target.value;

		const { credentials } = this.state;
		credentials[name] = value;
		this.validator.validate(name, value).then(() => {
			const { errorBag } = this.validator;
			this.setState({ errors: errorBag, credentials });
		});
	}
	handleSubmit(event) {
		event.preventDefault();
		const { credentials } = this.state;
		this.validator.validateAll(credentials).then(success => {
			if (success) {
				this.submit(credentials);
			}
		});
	}
	submit(credentials) {
			AuthService.resetPassword(credentials, function(response){
		   			if (response == true) {
					this.setState({ isSuccess: true });
					return;
				}
				const responseError = { isError: true, code: response.statusCode, text: response.error };
				this.setState({
					responseError: { isError: true, code: response.statusCode, text: response.error },
                });
         }.bind(this));
	}

	render() {
     	const isAuthenticated = this.state.isAuthenticated;
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        if (isAuthenticated) {
            return <Redirect to={from} replace />;
        }
		const { errors } = this.state;
		return <div className="auth-content">
				<Header />
				<div className="row">
					<div className="col-sm-6 col-sm-offset-1 form-box">
						{this.state.responseError.isError && <p className="White">
								<b>{this.state.responseError.text}</b>
							</p>}
						{this.state.isSuccess && <p>
								If the email you entered exists, a reset link has been sent !
							</p>}
						<div>
							<div className="form-top">
								<div className="form-top-left">
									<h3>
										<b className="title color">Reset your password</b>
									</h3>
								</div>
								<div className="form-top-right">
									<i className="fa fa-key" />
								</div>
							</div>
						</div>
						<div className="form-bottom">
							<form onSubmit={this.handleSubmit} className="login-form">
								<div className="form-group">
									<input name="email" placeholder="E-mail address" type="email" onChange={this.handleChange} className="form-control" required minLength={6} />
									{errors.has('email') && <p className="custom-error">
											{errors.first('email')}
										</p>}
								</div>

								<button className="btn" type="submit">
									Reset Password
								</button>
							</form>
						</div>
					</div>
				</div>
				<Footer />
			</div>;
	}
}

export default Forgot;
