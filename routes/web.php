<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'password'],function() {
	Route::post('/email', 'Auth\ForgotPasswordController@getResetToken');
	Route::post('/reset', 'Auth\ResetPasswordController@reset');
});


Route::group(['prefix'=> 'auth'],function(){
    Route::post('/register','Auth\RegisterController@register');
    Route::post("/login",'Auth\LoginController@login');
    Route::post("/logout",'Auth\LoginController@logout');
    Route::get("/check-login",'Auth\LoginController@checkLogin');
    Route::post('/sign/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','facebook|google');
});

Route::post('/save-message', 'MessagesController@saveMessage');
Route::post('/load-conversation', 'MessagesController@loadConversation');
Route::post('/new-conversation', 'MessagesController@newConversation');
Route::get('/load-user', 'MessagesController@loadUser');

Route::post('/delete-message-permanent', 'MessagesController@deleteMessagePermanent');
Route::post('/delete-message-sotf', 'MessagesController@deleteMessageSoft');
Route::post('/load-messages-by-id', 'MessagesController@loadMessagesByConversationId');
Route::post('/mark-as-seen', 'MessagesController@markAsSeen');

Route::get('/social/{social}','Auth\LoginController@socialLogin')->where('social','facebook|google');
Route::get('{slug}', function() {
    return view('home');
})->where('slug', '(?!api)([A-z\d-\/_.]+)?');


