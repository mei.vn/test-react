import React from 'react'
import {render} from 'react-dom'
// import { Router, Route, browserHistory } from 'react-router';
import { BrowserRouter, Route } from 'react-router-dom'
import login from './pages/auth/login';
import register from './pages/auth/register';
import home from './pages/auth/home';
import social from './pages/auth/social';
import forgot from './pages/auth/forgotPassword';
import reset from './pages/auth/resetPassword';
import chat from './pages/chat/chat';
render(
	<BrowserRouter>
		<div>
			{/* exact */}
			<Route path="/" component={home} exact />
			<Route path="/login" component={login}  />
			<Route path="/forgot-password" component={forgot}  />
			<Route path="/reset-password/:token/:email" component={reset}  />
			<Route path="/sign/:social" component={social} />
			<Route path="/register" component={register}  />
			<Route path="/chat" component={chat} />
		</div>
	</BrowserRouter>,
	document.getElementById('app')
);