<?php

namespace App\Http\Controllers;

use App\User;
use App\Messages;
use App\Conversations;
use DB;
use Auth;
use Illuminate\Http\Request;
use Redis;
class MessagesController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
        public function saveMessage(Request $request){
        $result = [];
        $messages = $request->all();
        $messages['created_at'] = \Carbon\Carbon::now();
         $messages['updated_at'] = \Carbon\Carbon::now();
        $result['data'] =  DB::table('messages')->insertGetId(
           $messages
        );
          $conver_id = $request->conversation_id;
          $channels = DB::table('conversations_users')->where('conversations_users.conversation_id',$conver_id )
          ->join('users', 'conversations_users.user_id', '=', 'users.id')
          ->pluck('users.channel');
          $channels_json = json_encode($channels);
         $messages['channel'] = $channels_json;
         $mess_res =  json_encode($messages);
         echo $mess_res ;
         $redis = Redis::connection();
         $redis->publish('message' ,$mess_res);
              //Mark as Unsee
        $cr_user = Auth::user()->id;
         $user_unseens = DB::table('conversations_users')->where('conversations_users.conversation_id',$conver_id )
         ->where('conversations_users.user_id', '<>', $cr_user)
         ->join('users', 'conversations_users.user_id', '=', 'users.id')
         ->pluck('users.id');

        foreach ($user_unseens as $key=>$value)
        {    
            $make_unseen = DB::table('conversations_users')
            ->where('conversation_id', $conver_id)
            ->where('user_id', $value)
            ->update(['seen' => 0]);

        }

    }
    public function loadConversation(){
            $user_id = Auth::user()->id;
        $conversations = DB::table('conversations')
            ->join('conversations_users', 'conversations_users.conversation_id', '=', 'conversations.id')
            ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
             ->where('conversations_users.user_id',  $user_id)
             ->select('conversations.id', 'conversations.conversation_name', 'messages.created_at', 'conversations_users.seen')
             ->orderBy('messages.created_at', 'DESC')
            ->groupBy('conversations.id','messages.created_at' )
            ->distinct()
            ->get();
            $arr_c = [];
            $new_arr = [];
            foreach($conversations as $key => $value){
            if(in_array($value->id, $arr_c)){
                unset($conversations[$key]);
                continue;
            }
            $arr_c [] = $value->id;
             $new_arr[] = $conversations[$key];
            }

        $result = -1;
        if(!empty( $new_arr)){
            $result = json_encode($new_arr, true);
        } 
         echo $result;

     
    }

    public function checkSingleConversation($user_list){
        $user_1 = $user_list[0];
        $user_2 = $user_list[1];
        // check list user 1, check list user 2 and map
        $conversations1 = DB::table('conversations')
            ->join('conversations_users', 'conversations_users.conversation_id', '=', 'conversations.id')
             ->where('user_id',  $user_1)->pluck('conversation_id');
        $conversations2 = DB::table('conversations')
            ->join('conversations_users', 'conversations_users.conversation_id', '=', 'conversations.id')
             ->where('user_id',  $user_2)
             ->whereIn('conversation_id',  $conversations1)
             ->get();
        //    $conversation =  DB::table('conversations')->where('conversation_name' , $name )->get();
         return $conversations2;
    }
       public function newConversation(Request $request){
    
           try {
            $result = ['message' => '', 'data' => false, 'status' => 'fail'];
            $result['status'] =  'success' ;
            $user_list = $request->user_id_array;
            $type = $request->type;
             $conversation_name = '';
            foreach($user_list as $val){
                 $conversation_name .= $val.' ';
            }
            $conversation_name = DB::table('users')->WhereIn('id',  $user_list)->select('id', 'name')->get(); 
            $conversation_name = json_encode($conversation_name , JSON_UNESCAPED_UNICODE);
            if($type == 0){
            $conversation = $this->checkSingleConversation( $user_list );
            if(count($conversation) != 0){
                    $result['message'] =  'Conversation exist!' ;
                    $result['data'] = $conversation[0]->id;
                    $result['name'] = $conversation[0]->conversation_name;
                    echo json_encode($result);
                    return;
                }
            }
            $conversation_id =  DB::table('conversations')->insertGetId(
                    ['conversation_name' =>  $conversation_name]
            );
            $cr_users = Auth::user()->id;
            foreach($user_list as $key => $value){
                if($cr_users == $value)
                {
                    $conversations_users =  DB::table('conversations_users')->insert(
                        ['conversation_id' => $conversation_id,'user_id' => $value, 'type' => $type, 'seen' => 1]
                );
                }
                else{
                    $conversations_users =  DB::table('conversations_users')->insert(
                        ['conversation_id' => $conversation_id,'user_id' => $value, 'type' => $type, 'seen' => 0]
                );
                }
         
            };
            $result['message'] =  'new' ;
            $result['data'] = $conversation_id;
            $result['name'] = $conversation_name;
            echo json_encode( $result);
            die;
        } catch (Exception $e) {
             echo  response()->json([
                "error" => "could_not_get_data",
                "message" => "Enable to process request.",
                'status' => 'fail'
            ], 422);
        }
           

    }
       public function loadUser(){
        $cr_users = Auth::user()->id;
        // echo $cr_users;
        $users = DB::table('users')->where('id', '<>', $cr_users)->get();
         $result = -1;
        if(!empty( $users)){
            $result = json_encode($users);
        } 
         echo $result;
        die;
    }
    public function loadMessagesByConversationId(Request $request){
        $conversation_id = $request->conversation_id;
        $page = $request->page * 10;
        $cr_user_id = Auth::user()->id;
        $messages = DB::table('messages')
        ->where('conversation_id', $conversation_id)
        ->where(function ($query) use ($cr_user_id) {
           return  $query->where('user_deleted_array', 'NOT LIKE', '%_'.$cr_user_id.'_%')
            ->orWhere('user_deleted_array', NULL)
            ->orWhere('user_deleted_array', 'NOT LIKE', '%_'.$cr_user_id.'_%');
        })
        ->orderBy('created_at', 'desc')->take($page)->get();
        echo json_encode( $messages);
    }
        public function deleteMessagePermanent(Request $request){
        $message_id = $request->message_id;
        $message_deleted = DB::table('messages')
        ->where('id', $message_id)
        ->delete();
        echo json_encode( $message_deleted);
    }
     public function deleteMessageSoft(Request $request){
          try{
         $message_id = $request->message_id;
        $user_id = $request->user_id;
        $message = Messages::find($message_id);
        $user_deleted = $message->user_deleted_array;
        // if( $user_deleted == '' || empty($user_deleted))
        // var_dump( $user_deleted);
         $user_deleted .= '_'.$user_id.'_';
    //    else $user_deleted .= $user_id.'_';
        // ->where('id', $message_id)
        // ->pluck('user_deleted_array');
        // get user deleted
        $message->user_deleted_array = $user_deleted;
        echo $message->save();
        }
        catch(Exception $e){
            echo  response()->json([
                "error" => "could_not_delete_data",
                "message" => "Enable to process request.",
                'status' => 'fail'
            ], 422);
        }

    }
    
    public function markAsSeen(Request $request){
        try{
            $conversation_id = $request->conversation_id;
            $user_id = $request->user_id;
            $make_seen = DB::table('conversations_users')
            ->where('conversation_id', $conversation_id)
            ->where('user_id', $user_id)
            ->update(['seen' => 1]);
        }
        catch(Exception $e){
            echo  response()->json([
                "error" => "could_not_get_data",
                "message" => "Enable to process request.",
                'status' => 'fail'
            ], 422);
        }
      


    }

    

}