import Http from '../Http';

export const loadConversation = callback => {
	Http.post('/load-conversation')
		.then(res => {
			// const result = res.data;
			callback(res.data);
		})
		.catch(err => {
			console.log(err);
			callback(err);
		});
};

export const loadMessagesByConversationId = (conversation_id,page, callback) => {
	Http.post('/load-messages-by-id', {conversation_id, page})
		.then(res => {
			// const result = res.data;
			callback(res.data);
		})
		.catch(err => {
			console.log(err);
			callback(err);
		});
};

export const deleteMessagePermanent = (message_id, callback) => {
	Http.post('/delete-message-permanent', {message_id})
		.then(res => {
			console.log(res);
			callback(res.data);
		})
		.catch(err => {
			console.log(err);
			callback(err);
		});
};
export const deleteMessageSoft = (message_id,user_id, callback) => {
			Http.post('/delete-message-sotf', { message_id, user_id })
				.then(res => {
					// const result = res.data;
					callback(res.data);
				})
				.catch(err => {
					console.log(err);
					callback(err);
				});
		};
export function newConversation(user_id_array,type, callback) {
			Http.post('/new-conversation', { user_id_array, type })
				.then(res => {
					console.log(res);
					callback(res.data);
				})
				.catch(err => {
					const statusCode = err.response.status;
					const data = { error: null, statusCode };
					data.error = err.response.data.message;
					callback(data);
				});
		}
export const loadUser = (callback) => {
	Http.get('/load-user')
		.then(res => {
			callback(res.data);
		})
		.catch(err => {
			console.log(err);
			callback(err);
		});
};

export function saveMessage(message, callback) {
	Http.post('/save-message', message)
		.then(res => {
			//    set data into localStorage
	
			// const result = res.data;

			callback(true);
		})
		.catch(err => {
			// const statusCode = err.response.status;
			// const data = { error: null, statusCode };
			// data.error = err.response.data.message;
			console.log(err);
			callback(err);
		});
}
export function markAsSeen(conversation_id, user_id, callback){
	Http.post('/mark-as-seen',  { conversation_id, user_id })
	.then(res => {
		callback(true);
	})
	.catch(err => {

		// const statusCode = err.response.status;
		// const data = { error: null, statusCode };
		// data.error = err.response.data.message;
		callback(err);
	});
}
