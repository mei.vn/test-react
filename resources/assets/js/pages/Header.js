import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import AuthService from '../services';
class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = { logout: false, isAuthenticated:AuthService.checkLogin() };
		this.handleLogout = this.handleLogout.bind(this);
	}
	handleLogout() {
		event.preventDefault();
		AuthService.logout(
			function(res) {
				if (res) this.setState({ logout: true });
			}.bind(this)
		);

		// return to login
	}
	render() {
		if (this.state.logout) {
			return <Redirect to="/login" replace />;
		}
		const isAuthenticated = this.state.isAuthenticated;
		const user = AuthService.getUserLogin();
		const display_login = isAuthenticated ? 'none' : '';
		const display_logout = isAuthenticated ? '' : 'none';
		const menu = this.props.menu;
		const link = this.props.link;
		return <span>
				<div className="header-menu contact-profile">
					<p className="logo">
						<img src="/images/backgrounds/logo.png" alt="true" />
					</p>
					<p style={{ marginLeft: 25 }}>
						<Link to="/">Home</Link>
					</p>
					<p style={{ marginLeft: 25, display : display_logout }}>
						<Link to="/chat">Chat</Link>
					</p>
					<div className="social-media">
						<i className="fa fa-power-off" aria-hidden="true" onClick={this.handleLogout} style={{ display: display_logout }} />
						<Link to="/Login" style={{ display: display_login }}>
							<i className="fa fa-sign-in" aria-hidden="true" /> Login
						</Link>
						<Link to="/register" style={{ display: display_login }}>
							<i className="fa fa-twitter" aria-hidden="true" style={{ display: display_login }} /> Sign Up
						</Link>
					</div>
					<div className="pull-right drop">
						<span style={{ display: display_logout }}>
							<p className="avt">
								<img src="images/girl.jpg" alt="avt" />
							</p>
							<div className="drop-info">
								{user && <p className="White">{user.username} !</p>}
							</div>
						</span>
					</div>
				</div>
			</span>;
	}
}
export default Header;
