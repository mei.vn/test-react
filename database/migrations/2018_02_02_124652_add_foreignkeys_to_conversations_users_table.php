<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysToConversationsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
 {
  Schema::table('conversations_users', function(Blueprint $table)
  {
    $table->foreign('user_id', 'fk_conversations_users_users')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
    $table->foreign('conversation_id', 'fk_conversations_users_convesations')->references('id')->on('conversations')->onUpdate('cascade')->onDelete('cascade');
    
  });
 }


 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::table('messages', function(Blueprint $table)
  {
   $table->dropForeign('fk_conversations_users_users');
   $table->dropForeign('fk_conversations_users_convesations');
  });
 }
}
