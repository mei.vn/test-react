import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import AuthService from '../../services';

class Social extends React.Component {
	constructor(props) {
		super(props);

	}
	componentWillMount() {
		this.setState({ isAuthenticated: AuthService.checkLogin() });
		const social = this.props.match.params.social;
		const params = this.props.location.search;
		if (params && social) {
			AuthService.socialLoginCallback({ params, social }, function(response) {
				if (response == true) {
					this.setState({ isAuthenticated: true });
				}
			else{
				const responseError = { isError: true, code: response.statusCode, text: response.error };
				this.setState({
					responseError: { isError: true, code: response.statusCode, text: response.error },
				});
		}
	}.bind(this));
	}
}
	render() {
		const checkLogin = this.state.isAuthenticated;
		if (checkLogin) {
			return <Redirect to={'/chat'} replace/>;
		}
		return <div />;
	}
}

export default Social;
