<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\TestCase
class LoginControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    // use DatabaseMigrations;
    /**
     *  @test
    **/
 


    public function test_login_wrong_should_return_error(){

          $credentials = array(
              'email' => 'invalid_email',
            'password' => 'invalid_pass',
            '_token' => csrf_token()
        );
         $response = $this->call('post', 'auth/login', $credentials);

        $result['error'] = "invalid_credentials";
        $result['message'] = "Email or password incorrect!";
        $result = json_encode($result);
       
        $this->assertEquals($result, $response->getContent());
    }

    public function test_login_success_should_return_user(){

          $credentials = array(
              'email' => 'thuong@mailinator.com',
            'password' => '123456',
            '_token' => csrf_token()
        );

        $response = $this->call('post', 'auth/login', $credentials);

        $res['user'] = Auth::user();
        $res['token'] = true;
        $res = json_encode($res);

        $this->assertEquals($res, $response->getContent());
    }



}
