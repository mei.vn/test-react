import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import ChatService from '../../chatService';
import $ from 'jquery';
class AllConversation extends React.Component {
	constructor(props) {
		super(props);
		this.state = { conversations: [] };
	}

	componentDidMount() {
		console.log(this.props.conversations);
	}
	render() {

					// list user [1=>abc, 2=>cdc]
			// var users = this.props.users;
			// var user_arr = [];
			// for (let user of users) {
			// 	user_arr[user.id] = user.name;
			// }
		var noti = this.props.noti;
		var conversations = this.props.conversations;
		var conversations_arr = [];
		var conversations_detail;
		if (conversations && conversations != '' && conversations != -1) {

		conversations = conversations.map(function(conversation, index) {
					// [2,1]
					var conversations_name = JSON.parse(conversation.conversation_name);
					var tmp_name = '';
					for (let vl_name of conversations_name) {
						if (vl_name.id != this.props.cr_user_id) {
							tmp_name += vl_name.name + '_';
						}
					}
					if (tmp_name.endsWith('_')) {
						tmp_name = tmp_name.substring(0, tmp_name.length - 1);
					}
					var active_class;
					if (conversation.id == noti || conversation.seen == 0) {
						active_class = 'contact unread';
					} else active_class = 'contact';

					return <li className={active_class} data-id={conversation.id} data-cv_name={conversation.conversation_name} onClick={this.props.action} key={index}>
							<div className="wrap">
								<span className="contact-status online" />
								<img src="images/boy2.jpg" alt />
								<div className="meta">
									<p className="name">{tmp_name}</p>
								</div>
							</div>
						</li>;
				}.bind(this));
				}
				else conversations = null;
return <span>
<div id="sidepanel" className="col-sm-3 sidepanel full-height">
<h3 className="side-title"> All conversations </h3>
<div id="contacts">
<ul>{conversations}</ul>
</div>
</div>
</span>;
}
}

export default AllConversation;
