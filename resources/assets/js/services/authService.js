import Http from '../Http'
export function login(credentials, callback) {
            Http.post('/auth/login', credentials)
                .then(res => {
					//    set data into localStorage
                    const result = res.data;
                    saveLogin(result);
                    callback(true);
								})
                .catch(err => {
                    const statusCode = err.response.status;
                     const data = { error: null, statusCode };
                       if (statusCode === 401 || statusCode === 422) {
							// status 401 means unauthorized
							// status 422 means unprocessable entity
							data.error = err.response.data.message;
						}
                   callback(data);
                })

}
export function socialLoginCallback(data, callback) {
	Http.post(`/auth/sign/${data.social}/callback${data.params}`)
		.then(res => {
			const result = res.data;

			saveLogin(result);
			callback(true);
		})
		.catch(err => {
			const statusCode = err.response.status;
			const data = { error: null, statusCode };
			if (statusCode === 401 || statusCode === 422) {
				data.error = err.response.data.message;
			}
			callback(data);
		});
}

export function resetPassword(credentials, callback) {
            Http.post('/password/email', credentials)
                .then(res => {
    
                    callback(true);
                })
                .catch(err => {
  		            const statusCode = err.response.status;
						const data = { error: null, statusCode };
						if (statusCode === 401 || statusCode === 422) {
							data.error = err.response.data.message;
						}
						callback(data);
					});
}
export function updatePassword(credentials, callback) {
            Http.post('/password/reset', credentials)
                .then(res => {
                    const statusCode = res.data.status;
                    if (statusCode == 202) {
                        const data = {
                            error: res.data.message,
                            statusCode,
                        }
                        callback(data);
                    }
                    else
                     callback(true);
                })
                .catch(err => {
                		const statusCode = err.response.status;
						const data = { error: null, statusCode };
						if (statusCode === 401 || statusCode === 422) {
							data.error = err.response.data.message;
						}
						callback(data);
					});
}

export function register(credentials, callback) {
                Http.post('/auth/register', credentials)
					.then(res => {
						callback(true);
					})
					.catch(err => {
						const statusCode = err.response.status;
						const data = { error: null, statusCode };
						if (statusCode === 401 || statusCode === 422) {
							// status 401 means unauthorized
							// status 422 means unprocessable entity
							data.error = err.response.data.message;
						}
						callback(data);
					});
}
export function saveLogin(result) {
	var u = result.user;
	var user = { id: u.id, email: u.email, username: u.name , channel: u.channel};
	localStorage.setItem('me', JSON.stringify(user));
	localStorage.setItem('', true);
	localStorage.setItem('is_login', true);
	localStorage.setItem('token', result.token);
}
// var check_login = false;
export const checkLoginApi = () => {
	// var check_login = false;
Http.get('/auth/check-login')
                .then(res => {
					if (res.data == 1) {
					localStorage.setItem('is_login', true) ;

					}
					else localStorage.setItem('is_login', false);
			})
                .catch(err => {
              localStorage.setItem('is_login', false);
	})
}

export function checkLogin(){
checkLoginApi();
return localStorage.getItem('is_login') === 'true';
}
// var check = checkLogin();
export function getUserLogin() {
return JSON.parse(localStorage.getItem('me'));
}
export function logout(callback) {
	  Http.post('/auth/logout')
			.then(res => {
				if(res.status == 200){
					localStorage.removeItem('jwt_token');
					localStorage.removeItem('me');
					localStorage.removeItem('token');
					localStorage.setItem('is_login', false);
					callback(true);
				}
			})
			.catch(err => {
				const statusCode = err.response.status;
				const data = { error: null, statusCode };
				data.error = err.response.data.message;
				callback(data);
			});
  
};
