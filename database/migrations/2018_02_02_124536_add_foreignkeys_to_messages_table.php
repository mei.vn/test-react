<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysToMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
 {
  Schema::table('messages', function(Blueprint $table)
  {
    $table->foreign('author_id', 'fk_messages_users')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
    $table->foreign('conversation_id', 'fk_messages_convesations')->references('id')->on('conversations')->onUpdate('cascade')->onDelete('cascade');
    
  });
 }


 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::table('messages', function(Blueprint $table)
  {
   $table->dropForeign('fk_messages_users');
   $table->dropForeign('fk_messages_convesations');
  });
 }
}
