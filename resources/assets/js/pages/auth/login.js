import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import AuthService from '../../services';
import Header from '../Header';
import Footer from '../Footer';
class Login extends React.Component {
    constructor(props) {
        super(props);
		this.state = { credentials: { email: '', password: '', remember: '' }, 
		responseError: { isError: false, code: '', text: '' },
		isAuthenticated : AuthService.checkLogin(),
     };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        const {credentials} = this.state;
        credentials[name] = value;

    }
    handleSubmit(event) {
        event.preventDefault();
        const {credentials} = this.state;
        this.submit(credentials);
    }
    submit(credentials) {
        AuthService.login(credentials, function(response){
        if(response == true){
              this.setState({ isAuthenticated : true});
        }
        const responseError = { isError: true, code: response.statusCode, text: response.error };
        this.setState({ responseError:{ isError: true, code: response.statusCode, text: response.error } });
        }.bind(this));
    }
    render() {
		const isAuthenticated = this.state.isAuthenticated;
        const { from } = this.props.location.state || { from: { pathname: '/chat' } };
        if (isAuthenticated) {
			return <Redirect to={from} />;
		}
        const {errors} = this.state;
        return <div>
				<div className="auth-content">
					<Header />
					<div className="row">
						<div className="col-sm-6 col-sm-offset-1 form-box">
							{this.state.responseError.isError && <p className="White">
									<b>{this.state.responseError.text}</b>
								</p>}
							<div>
								<div className="form-top">
									<div className="form-top-left">
										<h3>
											<b className="title color">Sign in</b> or
											<Link to="/register"> create an account?</Link>
										</h3>
									</div>
									<div className="form-top-right">
										<i className="fa fa-key" />
									</div>
								</div>
							</div>
							<div className="form-bottom">
								<form onSubmit={this.handleSubmit} className="login-form">
								<input type="hidden" name="channel" value="" />
									<div className="form-group">
										<input name="email" placeholder="E-mail address" onChange={this.handleChange} className=" form-control" type="email" required />
									</div>
									<div className="form-group">
										<input name="password" placeholder="Password" type="password" onChange={this.handleChange} className="form-control" required minLength={6} />
									</div>
									<div className="form-group">
										<input type="checkbox" onChange={this.handleChange} name="remember" value="1" />
										<label style={{ marginLeft: 10 }}> Remember me</label>
									</div>
									<p>
										<Link to="/forgot-password">Forgot your password?</Link>
									</p>
									<button className="btn" type="submit">
										Sign In
									</button>
								</form>
								<div className="social-login-buttons">
									<a className="btn btn-link-1 btn-link-1-facebook pull-left" href="/social/facebook">
										<i className="fa fa-facebook" /> Facebook
									</a>
									<a className="btn btn-link-1 btn-link-1-google-plus pull-right" href="/social/google">
										<i className="fa fa-google-plus" /> Google Plus
									</a>
								</div>
							</div>
						</div>
					</div>
					<Footer />
				</div>
			</div>;
    }
}


export default Login;
