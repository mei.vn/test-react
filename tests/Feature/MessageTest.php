<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use App\Messages;
use App\User;
use DB;

class MessageTest extends TestCase
{
    use DatabaseTransactions;
    protected $user;
    protected $password = '123456';
    /**
     * A basic test example.
     *
     * @return void
     */
  

     /** @before */
    public function setupUserObjectBeforeAnyTest() {
        parent::setUp();
        $this->user = factory(User::class)->create([
                'email' => 'john@example.com',
                'name' => 'thuongho',
                'password' => bcrypt($this->password),
            ]);
    }
           /** @test */
    public function it_save_message()
    {
        $message = new Messages(['body'=>'Body Message', 'author_id'=>1, 'conversation_id'=>'1']);
        $this->assertEquals('Body Message', $message->body);
    }
//     public function test_loadConversation_should_return_users_conversations(){
//             $user_id = $this->user->id;
//             $conversations = $this->call('post', '/load-conversation');
//         // $conversations = DB::table('conversations')
//         // ->join('conversations_users', 'conversations_users.conversation_id', '=', 'conversations.id')
//         // ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
//         //     ->where('conversations_users.user_id',  $user_id)
//         //     ->select('conversations.id', 'conversations.conversation_name', 'messages.created_at', 'conversations_users.seen')
//         //     ->orderBy('messages.created_at', 'DESC')
//         // ->groupBy('conversations.id','messages.created_at' )
//         // ->distinct()
//         // ->get();
//             $this->assertEquals('hihi', $conversations);

// }
public function test_newConversation_should_return_conversation_id(){
   $user_id = $this->user->id;
   $friend_user = factory(User::class)->create([
        'email' => 'john1@example.com',
        'password' => bcrypt($this->password),
        'name' => 'thuong'
]);
$requests = ['user_id_array' => [$user_id,$friend_user->id]];
$conversations = $this->call('post', '/new-conversation', $requests);
$user_1 = ["id" => $user_id, "name" => $this->user->name];
$user_2 = ["id" => $friend_user->id, "name" => "thuong"];
$conversation_name[] = $user_1;
$conversation_name[] = $user_2;
$conversation_name = json_encode($conversation_name);
//   $this->seeInDatabase('conversations', ['conversation_name' => $conversation_name ]);
 $this->assertEquals(1, $conversations->getContent());
}
    
}
